kitodo-docker
=============

Docker configurations for testing different versions and snapshots of Kitodo Production.

This uses docker-compose. There is a docker service for building Kitodo and one for running Kitodo.

Building Kitodo
---------------

There is a service for building a Kitodo Production release, ie. creating the war, zip, and config files.

Build the Kitodo Builder with docker-compose as follows:
```
$ docker-compose build kitodo
```

After that you can build a specific version of Kitodo:
```
$ docker-compose run kitodo <COMMIT> <SOURCE_URL>
```

You must specify either `<COMMIT>` or `<SOURCE_URL>`.
`<SOURCE_URL>` identifies from where the Kitodo source code will be downloaded.
If `<SOURCE_URL>` is omitted, the URL is constructed so that the standard kitodo repository on GitHub is taken.
You may either specify a complete URL pointing to a ZIP file or just the URL of the repository, eg.
`https://github.com/kitodo/kitodo-production`.

`<COMMIT>` is a Git commit hash. If specified, the source from the commit is taken.
If omitted, `master` is assumed.
If no URL or a base URL is given, then the download URL is constructed using `<COMMIT>`.
If a complete URL is given, however, the source from the URL is taken nonetheless.

The builder will create a directory named with the Kitodo API version and commit hash in
the `builds` directory. There, it places the files needed for deploying Kitodo Production:
- `kitodo-3.war`: The WAR file
- `kitodo-3-config.zip`: The subdirectories in `/usr/local/kitodo`
- `kitodo-3.sql`: The database dump
- `kitodo-3-modules.zip`: The modules
 

Running Kitodo
--------------

In the run directory there is the docker config for running a Kitodo instance

